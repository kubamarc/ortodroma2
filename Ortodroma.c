// Autor: Jakub Marcinkowski @kubamarc  
// 

#include <stdio.h>
#include <math.h>

// Funkcja licząca odległość między dwoma punktami
long double odleg(long double dlu1, long double szer1, long double dlu2, long double szer2) {
  if (dlu1 == 0 && szer1 == 0) {
    return 0;
  }

  long double ddlu = dlu2 - dlu1;

  long double pomocniczy = sin(szer1) * sin(szer2);
  pomocniczy += cos(szer1) * cos(szer2) * cos(ddlu);
  pomocniczy = acos(pomocniczy);
  long double promien = 111195; // Skorzystamy z wzoru na ortodromę. Niestety, ma on wadę
                                // Operujemy na doublach a ziemia jest geoidą, nie kulą, 
                                // co sprawia, że wynik może być nieco różny od rzeczywistości.

  long double wynik = promien * pomocniczy;

  return wynik;
}


int main(int argc, char *argv[]) {
  if (argc < 2) {
    printf ("Nie wczytano nazwy pliku\n");
    return 0;
  }

  long double dlu1 = 0.0, dlu2 = 0.0, szer1 = 0.0, szer2 = 0.0; // To będą pary współrzędnych do obliczania odległości między kolejnymi punktami
  long double dlugosc = 0; // Tu będziemy mierzyć długość trasy

  long double lat, lon;
  int prawda = 0;
  FILE *plik;
  plik = fopen(argv[1], "r");
  char c = '0';
  char slowo[] = {"<trkpt"};
  while (c != EOF) {
    c = fgetc(plik);
    if (c == slowo[prawda]) { // Ten "if" szuka wzoru zapisanego w tablicy "slowo"
      prawda++;               // "prawda" oznacza, który znak wzoru znaleźliśmy
    } else {
      prawda = 0;
    }
    if (prawda == 6) { // Jeżeli znaleźliśmy cały wzór, można wczytać resztę linijki, w tym liczby
      fscanf(plik, " lat=\"%Lf\" lon=\"%Lf\">", &lat, &lon);
      //printf("%Lf %Lf\n", lat, lon);
      dlu1 = dlu2;
      szer1 = szer2;
      dlu2 = lon;
      szer2 = lat;
      dlugosc += odleg(dlu1, szer1, dlu2, szer2);
      prawda = 0;
    }
  }
  fclose(plik);
  printf("Odległość wynosi około %Lf metrów czyli około %Lf kilometrów\n", dlugosc, dlugosc/1000);
  return 0;
}
